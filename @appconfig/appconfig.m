classdef appconfig < handle
% Application configuration.
%
% This class provides a mechanism to transparently store application or
% object related configuration data using standard Matlab data format.
% 
% USAGE
%
% A) obj = appconfig('Property1', value1, ...)
% 
% Create a configuration key-value pair storage object. 
%
% Properties:
%     Program       Program name (the configuration path is
%                   constructed based on the program name). This
%                   can be omitted if 'Path' is given.
%     Path          The configuration file path (can be omitted if
%                   program name is given).
%     File          File name (defaults to the program name if omitted).
%     AutoSave      Save automatically on every change of the 
%                   configuration (default: true).
%
% Configuration file name assembly:
% If the program name is supplied, the path will be in the
% respective folder (operating system dependent, e.g. applications
% folder on Windows, $HOME/.config/<program-name> on Linux, ...).
% The 'Path' is then relative to this. If 'Program' is omitted but
% 'Path' is given, 'Path' is the full path to the directory
% containing the configuration files. 'File' is only the file
% name in said configuration path.
%
% B) obj = appconfig(config-obj, path)
%
% Creates a sub-configuration object. The 'sub-config' holds a link
% to the base configuration object with a path. E.g.
%
%     module_config = config(program_config, 'my.module')

    properties
        AutoSave   % Automatically save on each change.
    end

    properties(SetAccess = protected)
        Path       % Configuration data path (empty for derived objects).
        File       % Configuration file name (empty for derived objects).
    end

    properties(SetAccess = private)
        Config     % Configuration data tree (or root config object).
        ConfigPath % Relative data path (in case this is a derived object).
    end

    methods
        function obj = appconfig(varargin)
            % Constructor.
            % See: appconfig
            if isa(varargin{1}, 'appconfig')
                % FIXME sanity check
                obj.Config = varargin{1};
                
                if obj.Config.issubconfig()
                    % sub-config relative to root config
                    obj.ConfigPath = [obj.Config.ConfigPath '.' varargin{2}];
                    obj.Config = obj.Config.Config;
                    
                    % make sure, parent config exists
                    obj.Config.init(obj.ConfigPath, struct());
                else
                    obj.ConfigPath = varargin{2};
                end
                
                obj.AutoSave = obj.Config.AutoSave;
            else
                opts = proplist.parse({...
                    { 'Program', 'program', [] }, ...
                    { 'Path', 'path', [] }, ...
                    { 'File', 'file', [] }, ...
                    { 'AutoSave', 'autosave', true }, ...
                }, true, varargin);
            
                % sanity checks
                if isempty(opts.path) && isempty(opts.program)
                    error('Neither ''Path'' nor ''Program'' property provided.')
                end

                if ~isempty(opts.path)
                    if ~isempty(opts.program)
                        if ~isrelpath(opts.path)
                            error('Expected ''Path'' to contain a relative path.')
                        end
                    else
                        if isrelpath(opts.path)
                            error('Expected ''Path'' to be an absolute path.')
                        end
                    end
                end

                % assemble configuration path
                if ~isempty(opts.program)
                    obj.Path = fullfile(sysconfdir(), opts.program);
                end

                if ~isempty(opts.path)
                    if ~isempty(opts.program)
                        obj.Path = fullfile(obj.Path, opts.path);
                    else
                        obj.Path = opts.path;
                    end
                end

                % create path if necessary
                if ~exist(obj.Path, 'dir')
                    if exist(obj.Path, 'file')
                        error(['Invalid configuration path: ' obj.Path ' is a file']);
                    end

                    mkdir_r(obj.Path);
                end

                % file name
                if ~isempty(opts.file)
                    obj.File = opts.file;
                else
                    if ~isempty(opts.program)
                        obj.File = [opts.program '.rc'];
                    else
                        obj.File = 'config.rc';
                    end
                end

                % other data
                obj.AutoSave = opts.autosave;
                obj.ConfigPath = [];

                % load data if it exists
                obj.Config = struct();

                if exist(obj.fullfile(), 'file')
                    obj = obj.load();
                end
            end
        end
        
        function save(self)
        % Save configuration.
            if self.issubconfig()
                self.Config.save();
            else
                Config = self.Config; %#ok<PROP>
                save(self.fullfile(), 'Config', '-mat');
            end
        end
        
        function self = load(self)
        % Load configuration from file.
            if self.issubconfig()
                self.Config = self.Config.load();
            else
                tmp = load(self.fullfile(), '-mat');
                self.Config = tmp.Config;
            end
        end
    
        function clear(self, key)
        % Clear entry or configuration.
            if nargin < 2
                self.Config = struct();
            else
                if ~issubconfig(self)
                    self.Config = remove_node(self.Config, strsplit(key, '.'));
                else
                    self.Config.Config = remove_node(self.Config.Config, strsplit([self.ConfigPath '.' key], '.'));
                end

                if self.AutoSave
                    self.save();
                end
            end
        end
        
        function value = get(self, key, default)
        % Get a configuration value (with optional default value).
            % default value?
            if ~issubconfig(self)
                cfg = self.Config;
                if nargin < 2
                    nodes = {};
                else
                    nodes = strsplit(key, '.');
                end
            else
                cfg = self.Config.Config;
                if nargin < 2
                    nodes = strsplit([self.ConfigPath], '.');
                else
                    nodes = strsplit([self.ConfigPath '.' key], '.');
                end
            end

            % recurse
            for i = 1:size(nodes, 2)-1
                if isfield(cfg, nodes{i}) && ~isempty(cfg.(nodes{i}))
                    cfg = cfg.(nodes{i});
                elseif nargin == 3
                    value = default;
                    return;
                else
                    warning(['A sub-path in the config ' self.fullfile() ' does not exist: ' key]);
                    value = [];
                    return
                end
                
                % sanity check
                if ~isstruct(cfg)
                    cur = nodes{1};
                    for k = 2:i
                        cur = [cur '.' nodes{k}]; %#ok<AGROW>
                    end
                    
                    error(['Invalid configuration while accessing ''' key ''': ''' cur ''' is not a struct.' ])
                end
            end
            
            if isempty(nodes)
                value = cfg;
            elseif isfield(cfg, nodes{end})
                value = cfg.(nodes{end});
            elseif nargin == 3
                self.set(key, default);
                value = default;
            else
                warning(['A sub-path in the config ' self.fullfile() ' does not exist: ' key]);
                value = [];
            end
        end
        
        function self = set(self, key, value)
        % Set configuration value.
            if ~issubconfig(self)
                self.Config = set_node(self.Config, strsplit(key, '.'), value);
            else
                self.Config.Config = set_node(self.Config.Config, strsplit([self.ConfigPath '.' key], '.'), value);
            end
            
            if self.AutoSave
                self.save();
            end
        end
        
        function res = isempty(self)
            if ~issubconfig(self)
                res = isempty(self.Config);
            else
                cfg = self.Config.Config;
                nodes = strsplit([self.ConfigPath], '.');                
                
                for i = 1:size(nodes, 2)
                    if isfield(cfg, nodes{i})
                        cfg = cfg.(nodes{i});
                    else
                        % FIXME shouldn't happend?
                        res = true;
                        return
                    end
                end
                
                res = isempty(cfg);
            end
        end
        
        function self = init(self, key, default)
            % Initialize configuration with a value.
            
            set(self, key, get(self, key, default));
        end
        
        function res = ismodified(self)
        % Check if configuration was modified (with respect to saved
        % configuration).
            if self.issubconfig()
                res = self.Config.ismodified();
            else
                tmp = load(self.fullfile(), '-mat');
                
                res = isequal(tmp.Config, self.Config);
            end
        end
    end
    
    methods % UTILITIES
        function f = fullfile(self)
        % Name of the configuration file (absolute path).
            f = fullfile(self.Path, self.File);
        end
        
        function r = issubconfig(self)
        % Check if this is a sub-config.
            r = isempty(self.File);
        end
        
        function show(self)
            fprintf(1, 'Configuration tree:\n');
            show_node(self.Config, '    ');
        end
    end
end

function cdir = sysconfdir()
    if ispc
        cdir = getenv('LOCALAPPDATA');
        if isempty(cdir) % older than Vista
            cdir = getenv('APPDATA');
            if isempty(cdir)
                error('Internal error: could not detect application data directory.');
            end
        end
    elseif isunix
        cdir = fullfile(getenv('HOME'), '.config');
    elseif ismac
        warning('Configuration path detection has not been verified for MAC');
        cdir = fullfile(getenv('HOME'), 'Library', 'Preferences');
    else
        error('Unsupported operating system')
    end
end

function r = isrelpath(path)
    if ispc
        r = path(2) ~= ':';
    elseif isunix
        r = path(1) ~= '/';
    elseif ismac
        warning('Relative path detection has not been verified for MAC');
        r = path(1) ~= '/';
    else
        error('Unsupported operating system')
    end
end

function node = set_node(node, keys, value)
    if size(keys, 2) == 1
        % sanity check
        if isfield(node, keys{1}) && isstruct(node.(keys{1}))
            if ~isstruct(value)
                fullkey = keys{1}; % FIXME need to find the full key for better error reporting
                error(['Error in config: trying to change a config node to a value (node ''' fullkey ''')']) 
            else
                % FIXME how to sanity check, if this does not mess up the config tree?
            end
        end
        
        % set entry
        node.(keys{1}) = value;
    else
        % sanity check
        if ~isfield(node, keys{1})
            % auto-generate tree node
            node.(keys{1}) = struct();
        elseif ~isstruct(node.(keys{1}))
            fullkey = keys{1}; % FIXME need to find the full key for better error reporting
            error(['Error in config: trying to change a value node to a config node (node ''' fullkey ''')']) 
        end            
        
        % recurse
        nextnode = node.(keys{1});
        node.(keys{1}) = set_node(nextnode, keys(2:end), value);
    end
end

function node = remove_node(node, keys)
    if size(keys, 2) == 1
        node = rmfield(node, keys{1});
    else
        % sanity check
        if ~isfield(node, keys{1})
            % auto-generate tree node
            node.(keys{1}) = struct();
        elseif ~isstruct(node.(keys{1}))
            fullkey = keys{1}; % FIXME need to find the full key for better error reporting
            error(['Error in config: trying to change a value node to a config node (node ''' fullkey ''')']) 
        end            
        
        % recurse
        nextnode = node.(keys{1});
        node.(keys{1}) = remove_node(nextnode, keys(2:end));
    end
end

function show_node(node, indent)
    fields = fieldnames(node);
    for k = 1:length(fields)
        fprintf(1, '%s.%s', indent, fields{k});
        next = node.(fields{k});
        if isstruct(next)
            fprintf(1, '\n');
            show_node(next, [indent '    ']);
        else
            if isnumeric(next) || islogical(next)
                % FIXME deal with arrays
                fprintf(1, ' = %g\n', next);
            elseif ischar(next)
                fprintf(1, ' = ''%s''\n', next);
            elseif iscell(next)
                fprintf(1, ' =  { <cell-contents hidden> } \n');
            else
                fprintf(1, ' = <contents cannot be displayed>\n');
            end
        end
    end
end

function mkdir_r(path)
    % note: matlab's mkdir is recursive
    [s,mess,messid] = mkdir(path);
    if ~s
        error([messid ': ' mess]);
    end
end