classdef proplist < handle
    % PROPLIST - Property handling and parsing.
    % 
    % This class, or the static proplist.parse allows parsing of extended 
    % properties, ie. properties which can be set with a long and a short name.
    %
    % USAGE
    %
    % 1) Parsing property lists.
    %
    %      arg = proplist.parse(argdef, varargin)
    %      arg = proplist.parse(argdef, defaults, varargin)
    %
    %    with
    %      argdef          The argument parser definition (see below).
    %      varargin        Optional list of property-value pairs.
    %      defaults        A boolean value if default values are always assigned.
    %
    % 2) As base class
    %
    %      obj@proplist.parse(classname, argdef, varargin)
    %
    %    with
    %      classname       The classname where the proplist constructor is called.
    %
    %
    % classdef myclass < handle & proplist
    %    methods
    %        function obj = myclass(varargin)
    %            obj@proplist(?myclass, argdef, varargin)
    %            ...
    %        end
    %    end
    %    %...
    % end
    %
    % DEFINITIONS
    %
    %   Argument definition (argdef) is a cell array with entries
    %           { 'longname', 'fieldname', default-value }
    %   or
    %           { 'longname', 'fieldname', default-value, required }
    %   where
    %       longname        A longer possibly more descriptive option name.
    %       fieldname       Field or property the value is assigned to.
    %       default-value   The default value ([] for unassigned)
    %       required        Boolean value if the option is required
    %                       (default is false).
    
    %%% Constructor
    methods
        function obj = proplist(meta, argdef, varargin)
            % Constructor.
            % see proplist
            
            if ~isa(meta, 'meta.class')
                error('Invalid arguments, first argument must be of type ''meta.class''.');
            end
            
            % find parent property lists
            parents = meta.SuperclassList;
            
            for k = 1:length(parents)
                parentdef = proplist.proplistDefinitions(parents(k).Name);
                if ~isempty(parentdef)
                    argdef = {parentdef{:} argdef{:}}; %#ok<CCAT>
                end
            end
            
            % assign property list
            proplist.proplistDefinitions(meta.Name, argdef);
            
            % parse
            if isa(obj, meta.Name)
                proplist.parse(obj, varargin{:});
            end
        end
        
        function res = property(self, key, val)
            % property - Get or set a property value.
            
            if nargin < 2 || nargin > 3
                error('Invalid number of inputs.');
            end
            
            % split key into property-name and property-index
            if strfind(key, '[')
                tmp = strsplit(key, '[');
                key = tmp{1};
                keyindex = sscanf(tmp{2}, '%d');
            else
                keyindex = [];
            end
            
            % find real property name
            if ~isprop(self, key)
                argdef = proplist.proplistDefinitions(self);
                
                for k = 1:length(argdef)
                    propdef = argdef{k};

                    longname = propdef{1};
                    fieldname = propdef{2};
                    
                    if strcmp(key, longname)
                        key = fieldname;
                        break;
                    end
                end
            end
          
            if ~isempty(keyindex)
                tmp = self.(key);
                if nargin == 2 % get
                    res = tmp{keyindex};
                elseif nargin == 3 % set
                    tmp{keyindex} = val;
                    self.(key) = tmp;
                end
            else
                if nargin == 2 % get
                    res = self.(key);
                elseif nargin == 3 % set
                    self.(key) = val;
                end
            end
        end
        
        function setParameter(self, key, value)
            warning('This function is deprecated.')
            self.property(key, value);
        end
        
        function res = getParameter(self, key)
            warning('This function is deprecated.')
            res = self.property(key);
        end
    end
    
    methods(Static)
        function res = parse(argdef, varargin)
            % proplist.parse - Parse property list.
            % see proplist
            
            % expand varargin if necessary
            if length(varargin) == 1 && ~islogical(varargin{1})
                varargin = varargin{:};
            end

            % assign default values?
            setdefaults = false;
            if ~isempty(varargin)
               if ~ischar(varargin{1})
                   setdefaults = varargin{1};
                   
                   if iscell(varargin{2})
                       varargin = varargin{2:end};
                   else
                       varargin = varargin(2:end);
                   end
               end
            end
            
            % check if setting class properties
            if ~iscell(argdef)
                res = argdef;
                argdef = proplist.proplistDefinitions(argdef);
            end
            
            % parse properties
            for k = 1:length(argdef)
                propdef = argdef{k};

                longname = propdef{1};
                fieldname = propdef{2};
                defaultvalue = propdef{3};
                if length(propdef) > 3
                    required = propdef{4};
                else
                    required = false;
                end

                % find index in varargin
                varargidx = 0;
                for n = 1:2:length(varargin)
                    if strcmp(varargin{n}, fieldname) || strcmp(varargin{n}, longname)
                        varargidx = n;
                        break;
                    end
                end
                
                if varargidx > 0
                    % found property, set and remove from varargin
                    res.(fieldname) = varargin{varargidx+1};
                    varargin = { varargin{1:varargidx-1} varargin{varargidx+2:end} };
                elseif required
                    error(['Parameter ''' longname ''' is required.']);
                elseif ~isempty(defaultvalue) || setdefaults
                    res.(fieldname) = defaultvalue;
                end
            end

            if ~isempty(varargin)
                error(['Argument ''' char(varargin{1}) ''' is not known.']);
            end
        end
    end
    
    methods(Static, Hidden)
        function def = proplistDefinitions(classname, def)
            persistent proplistData;
            
            % get classname
            if ~ischar(classname)
                classname = class(classname);
            end
            
            % fix classname
            classname = regexprep(classname, '[.]', '___');
                    
            % get or set
            if nargin == 1
                if isfield(proplistData, classname)
                    def = proplistData.(classname);
                else
                    def = [];
                end
            elseif nargin == 2
                proplistData.(classname) = def;
            end
        end
    end
    
end