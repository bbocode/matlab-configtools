# Configuration, properties and other tools. #

Set of tools for managing application configurations, properties parsing 
and class helpers. 

## Tools ##

### appconfig ###

A class to manage application configuration.

### proplist ###

Property list parsing as base class proplist or with static method proplist.parse.

### tagged ###

A base class for 'tagged' objects (objects having a property "Tag", uses proplist).

### optdialog ###

An "optional" dialog (standard dialog with optional "do not show again" check box,
uses appconfig).