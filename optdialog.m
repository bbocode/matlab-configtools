function varargout = optdialog(varargin)
% OPTDIALOG --- "Optional" dialog.
%
% This dialog has an extra checkbox 'never show dialog again'. If this
% checkbox is set, the dialog will not be shown again and the previously
% selected answer will be returned.
%
% USAGE
%    res = optdialog('Property1', <property-1-value>, ...)
%
% PROPERTIES
%   Type            Type of dialog ('info', 'question', 'help', 'warning',
%                   'error').
%   Title           The short error message.
%   Text            The longer explanation.
%   Buttons         The button definition {{'<label>' (default: {{'Ok', 'default'}, {'Cancel', 'escape'}}).
%   Width           Width and
%   Height          height of the dialog window in pixels.
%   Program         The program name (see appconfig)
%   ID              Dialog ID (for appconfig).

% Last Modified by GUIDE v2.5 26-Nov-2015 16:08:49

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @optdialog_OpeningFcn, ...
                   'gui_OutputFcn',  @optdialog_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function optdialog_OpeningFcn(hObject, eventdata, handles, varargin)
%%% Default settings
handles.output = '';

%%% Parse arguments.
program = '';
handles.dialogid = 'dialog';
show = true;
type = 'info';
buttons = {{'Ok', 'default'}, {'Cancel', 'escape'}};
width = 400; % Dialog width.
height = 200; % Dialog height.

for k = 1:2:length(varargin)
    switch lower(varargin{k})
        case 'title'
            set(handles.heading, 'String', sprintf(varargin{k+1}));
            set(handles.dialog, 'Name', sprintf(varargin{k+1}));
        case 'text'
            set(handles.text, 'String', sprintf(varargin{k+1}));
        case 'type'
            type = lower(varargin{k+1});
        case 'buttons'
            buttons = varargin{k+1};
        case 'width'
            width = varargin{k+1};
        case 'height'
            height = varargin{k+1};
        case 'program'
            program = varargin{k+1};
        case 'id'
            dialogid = regexprep(varargin{k+1}, '[:]', '.');
            dialogid = regexprep(varargin{k+1}, '[^a-zA-Z0-9_.]', '.');
            handles.dialogid = dialogid;
        otherwise
            error(['Unknown property ''' varargin{k} '''.']);
    end
end

%%% Configuration
if ~isempty(program)
    handles.config = appconfig('Program', program, 'File', 'dialogs.ini', 'AutoSave', true);
    show = handles.config.get([handles.dialogid '.show'], true);
    if ~show
        handles.output = handles.config.get([handles.dialogid '.result']);
        guidata(hObject, handles);
        return
    end
end

%%% Set buttons.
nbuttons = length(buttons);

if nbuttons == 1
    buttons = {{buttons{1}{1}, 'default'}};
    handles.output_escape = buttons{1}{1};
end

for k = 1:nbuttons
    % check settings
    labletext = buttons{k}{1};
    if size(buttons{k},2) > 1
        what = buttons{k}{2};
        
        switch lower(what)
            case 'default'
                handles.output_default = labletext;
                default_button = handles.(sprintf('button%d',k));
            case 'escape'
                handles.output_escape = labletext;
        end
    else
        what = '';
    end
    
    % setup gui
    h = handles.(sprintf('button%d',k));
    set(h, 'Visible', 'on');
    set(h, 'String', labletext);
    switch lower(what)
        case 'default'
            set(h, 'FontWeight', 'bold');
    end
end

for k = nbuttons+1:3
    h = handles.(sprintf('button%d',k));
    set(h, 'Visible', 'off');
end

%%% Recompute size of dialog.
% dialog
p = get(handles.dialog, 'Position');
p = [p(1) p(2) width height];
set(handles.dialog, 'Position', p);

% panels
set(handles.bottompanel, 'Position', [ 0 0 width 80 ]);
set(handles.dialogpanel, 'Position', [ 0 80 width height-80]);

% bottom panel
p = [width-110 10 100 30];
set(handles.button1, 'Position', p);
p(1) = p(1)-110;
set(handles.button2, 'Position', p);
p(1) = p(1)-110;
set(handles.button3, 'Position', p);
set(handles.nevershow, 'Position', [10 55 width-20 17]);

% top panel
topheight = height - 80;
set(handles.icon, 'Position', [10 topheight-58 48 48]);
set(handles.heading, 'Position', [68 topheight-58 width-78 48]);
topheight = topheight-78;
if topheight < 0
    topheight = 0;
end
set(handles.text, 'Position', [10 10 width-20 topheight]);

%%% Determine the position of the dialog
% - centered on the callback figure
% if available, else, centered on the screen
FigPos=get(0,'DefaultFigurePosition');
OldUnits = get(hObject, 'Units');
set(hObject, 'Units', 'pixels');
OldPos = get(hObject,'Position');
FigWidth = OldPos(3);
FigHeight = OldPos(4);
if isempty(gcbf)
    ScreenUnits=get(0,'Units');
    set(0,'Units','pixels');
    ScreenSize=get(0,'ScreenSize');
    set(0,'Units',ScreenUnits);

    FigPos(1)=1/2*(ScreenSize(3)-FigWidth);
    FigPos(2)=2/3*(ScreenSize(4)-FigHeight);
else
    GCBFOldUnits = get(gcbf,'Units');
    set(gcbf,'Units','pixels');
    GCBFPos = get(gcbf,'Position');
    set(gcbf,'Units',GCBFOldUnits);
    FigPos(1:2) = [(GCBFPos(1) + GCBFPos(3) / 2) - FigWidth / 2, ...
                   (GCBFPos(2) + GCBFPos(4) / 2) - FigHeight / 2];
end
FigPos(3:4)=[FigWidth FigHeight];
set(hObject, 'Position', FigPos);
set(hObject, 'Units', OldUnits);

% Show a question icon from dialogicons.mat - variables questIconData
% and questIconMap
load dialogicons.mat
 
type = lower(type);
switch lower(type)
    case 'error'
    case 'warning'
        type = 'warn';
    case 'question'
        type = 'quest';
    case 'help'
    case 'info'
        type = 'lightbulb';
    otherwise
        type = 'lightbulb';
end

eval(['IconData = ' type 'IconData;']);
eval(['IconCMap = ' type 'IconMap;']);
eval(['IconCMap(uint32(IconData(1,1))+1,:) = get(handles.dialog, ''Color'');']);

Img=image(IconData, 'Parent', handles.icon);

set(handles.icon, ...
    'Visible', 'off', ...
    'YDir'   , 'reverse', ...
    'XLim'   , get(Img,'XData'), ...
    'YLim'   , get(Img,'YData'));

colormap(handles.icon, IconCMap);

% Make the GUI modal
set(handles.dialog,'WindowStyle','modal')

% update guidata
guidata(hObject, handles);

% UIWAIT makes optdialog wait for user response (see UIRESUME)
if show,
    set(handles.dialog, 'Visible', 'on');
    drawnow;
    
    uicontrol(default_button)
    uiwait(handles.dialog);
end

function varargout = optdialog_OutputFcn(hObject, eventdata, handles)
handles.config.set([handles.dialogid '.result'], handles.output);
varargout{1} = handles.output;
delete(handles.dialog);

function dialog_CloseRequestFcn(hObject, eventdata, handles)
if isequal(get(hObject, 'waitstatus'), 'waiting')
    uiresume(hObject);
else
    delete(hObject);
end

function dialog_KeyPressFcn(hObject, eventdata, handles)
switch get(hObject,'CurrentKey')
    case 'escape'
        handles.output = handles.output_escape;
        guidata(hObject, handles);
        uiresume(handles.dialog);
end
        
function nevershow_Callback(hObject, eventdata, handles)
show = handles.config.set([handles.dialogid '.show'], ~get(hObject, 'Value'));

function button1_Callback(hObject, eventdata, handles)
handles.output = get(hObject,'String');
guidata(hObject, handles);
uiresume(handles.dialog);

function button2_Callback(hObject, eventdata, handles)
handles.output = get(hObject,'String');
guidata(hObject, handles);
uiresume(handles.dialog);

function button3_Callback(hObject, eventdata, handles)
handles.output = get(hObject,'String');
guidata(hObject, handles);
uiresume(handles.dialog);

function button1_KeyPressFcn(hObject, eventdata, handles)
switch get(handles.dialog,'CurrentKey')
    case 'return'
        button1_Callback(hObject, eventdata, handles)
end
dialog_KeyPressFcn(handles.dialog, eventdata, handles);

function button2_KeyPressFcn(hObject, eventdata, handles)
switch get(handles.dialog,'CurrentKey')
    case 'return'
        button2_Callback(hObject, eventdata, handles)
end
dialog_KeyPressFcn(handles.dialog, eventdata, handles);

function button3_KeyPressFcn(hObject, eventdata, handles)
switch get(handles.dialog,'CurrentKey')
    case 'return'
        button3_Callback(hObject, eventdata, handles)
end
dialog_KeyPressFcn(handles.dialog, eventdata, handles);
