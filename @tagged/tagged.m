classdef tagged < handle & proplist
    % tagged - Base class for tagged (property 'Tag') objects.
    
    %%% Constructor
    methods
        function obj = tagged()
            % Constructor.
            % See: opsim.source.ray
            obj@proplist(?tagged, {{'Tag', 'Tag', []}}, []);
        end
    end
    
    %%% Properties
    properties
        Tag      % Tag string.
    end
    
    %%% Methods
    methods(Static)
        function obj = autotag(obj, tagbase)
            % tagged.autotag - Generate automatic tag.
            persistent db
            
            if nargin < 2
                tagbase = 'tag';
            end

            % tag object
            if isempty(obj.Tag)
                % generate tag number 
                if ~isfield(db, tagbase)
                    db.(tagbase) = 1;
                else
                    db.(tagbase) = db.(tagbase) + 1;
                end

                obj.Tag = sprintf('%s%d', tagbase, db.(tagbase));
            end
            
        end
    end
end
