function example_proplist(varargin)
    % Example: parse arguments of the form 'Property', property_value, ...

    args = proplist.parse({...
        {'Power', 'power', 'off', true}, ... % "Power" is required!
        {'Volume', 'volume', 11 }, ...       % All other properties are optional.
        {'Bass', 'bass', 0.0}, ...
        {'Treble', 'treble', 0.0}, ...
    }, ...
    true, ... % all field are created
    varargin);

    % show data:
    fprintf(1, 'args.power = %s\n', args.power);
    fprintf(1, 'args.volume = %g\n', args.volume);
    fprintf(1, 'args.bass   = %g\n', args.bass);
    fprintf(1, 'args.treble = %g\n', args.treble);
    
    % Example: classes (see examples.mybaseclass and examples.myclass; watch console output)
    mc1 = examples.myclass()
    
    mc2 = examples.myclass('Tag', 'myclass:1')
    
    mc2 = examples.myclass('Tag', 'myclass:1', 'Value', -1.0, 'Something', {})
end