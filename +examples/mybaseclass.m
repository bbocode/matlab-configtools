classdef mybaseclass < handle & proplist & tagged
    properties
        v                                   % Value
    end
    
    methods 
        function obj = mybaseclass()        % base classes do not handle 'varargin'
            obj@tagged;                     % construct base class 'tagged'
            
            obj@proplist(?examples.mybaseclass, ...     % meta-class of this class
                {...                                    % proplist definition
                    {'Value', 'v', 1.0}, ...
                });
        end
    end
end