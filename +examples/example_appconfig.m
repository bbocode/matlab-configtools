function example_appconfig()
    % Applications usually have a global variable, where application data is stored:
    globdata = struct();

    % Load config:
    globdata.config = appconfig('Program', 'MyAppConfigTest');
    
    % Example: program initialization
    if ~globdata.config.get('initialized', false)
        fprintf(1, 'Program is run for the first time, initializing...\n');
        globdata.config.set('initialized', true);
    else
        fprintf(1, 'Program initialization was already done.\n');
    end
    
    % Example: sub-module
    % If an application has sub-modules, a 'sub-configuration' can be created:
    modcfg = appconfig(globdata.config, 'modules.my');
    modcfg.set('version', 2.0);
    
    % show config
    globdata.config.show()
end