classdef myclass < handle & proplist & examples.mybaseclass
    properties
        something   % Something
    end
    
    methods 
        function obj = myclass(varargin)
            obj@examples.mybaseclass;      % construct base class 'mybaseclass'
            
            obj@proplist(?examples.myclass, ...     % meta-class of this class
                {...                                % proplist definition
                    {'Something', 'something', 'else'}, ...
                }, varargin);
        end
    end
end
